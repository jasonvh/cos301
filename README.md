# Readme

* **Module:** COS 301 - University of Pretoria
* **Year:** 2019
* **Topic:** Continuous Integration, Delivery & Deployment (CI/CD)


## Welcome

Welcome to the COS 301 CI/CD lecture. This repo is meant to serve as a fun introduction to CI/CD concepts. CI/CD form a vital component of the Software Engineering toolkit. 


## The Software Engineering Process

CI/CD form part of every step in the Software Engineering Process (SEP). In broad terms, we recommend at least the following 6 stages of the SEP:

1. **Requirements:** During this phase requirement solicitation is done. A proposal for a solution is formulated.
2. **Design:** The design of the solution is formulated. This could be from architecture to UI/UX.
3. **Implementation:** These are development sprints, often related to some semantic versioning such as vX.Y.Z. (See [https://semver.org](https://semver.org)). Development sprints can be agile.
4. **Testing:** Formal testing of sprints in some development or QA environment takes place. 
5. **Delivery:** This stage envolves the deployment to a production environment. 
6. **Maintenance:** This phase envolves continuous support on the solution in production.


## What is CI/CD?

The following definitions are sourced from [Atlassian](https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment).


### Continuous Integration

During Continuous Integration an automated process is implemented to merge together the contributions made by multiple members of the project. Merging of contributions are done as often as possible and as soon as a small scope of work is completed. This process puts emphasis on automated testing to ensure that any merged contributions do not break any functionality in the master code base. 

### Continuous Delivery

Continuous Delivery is the strategic roadmap formulated that defines when new features are released to customers. This is a semi-automatic process. Semver is an important component here as it helps tag major, minor and patched releases. It is up to the product management team to devise how and when these versions are released to the public. The frequency of releases is on daily, weekly or monthly basis.

### Continuous Deployment

Continuous Deployment is an extension of Continuous Delivery with the exception that updates are provided to the public as soon as some scope of work successfully passes the entire CI/CD pipeline. In this manner, frequent small updates are done on a daily or even hourly basis. In order to scale to this frequency of deployment, this process has to be fully automated. 

Read this: [https://techbeacon.com/devops/10-companies-killing-it-devops](https://techbeacon.com/devops/10-companies-killing-it-devops). Very interesting article that talks about the DevOps success of some big companies like Amazon and Netflix.



# Practical

The intent of the practical is to give students a guided hands-on practical experience on CI/CD as it is used in real-world scenarios. Students are given this opportunity to experiment with CI/CD and hopefully be able to use CI/CD as a crucial part in the SEP during the final year projects.

The practical consists out of 5 tasks. Each task is builds on the one before, with a little bit more increasingly difficult elements added.
If there are any questions or issues, please feel free to ask them or log them in [Issues](https://gitlab.com/Compiax/cos301/issues) and a team member of Compiax will get to it as soon as possible.


## Requirements

This practical requires some knowledge about `Git`, `GitHub/GitLab`, `Travis/GitLab-CI` and `Python`. Make sure you are comfortable with your terminal.


## Task 1

1. **Fork the repo**

In order to get you up and running, you need to fork the repo. This can be done by clicking on the fork button at the top of the repo, or just by clicking on this link: [Fork](https://gitlab.com/Compiax/cos301/forks/new)

2. **Create a branch for task 1**

In your terminal, run:

```sh
cd /path/to/repo
git checkout -b feat/task1
```

3. **Add yourself as a contributor**

Make sure to view the [CONTRIBUTING.md](https://gitlab.com/Compiax/cos301/blob/2019/CONTRIBUTING.md) file. You will see there is a list of contributors. 
Now, add yourself to the list.

4. **Status**

Firstly, run

```sh
git status
```

Here you will see the unstaged modifications to the `CONTRIBUTING.md` file. You first need to stage these changes for the commmit.

5. **Stage**

This can be done as follows:

```sh
git add CONTRIBUTING.md
```

Rerun the status check as above to see the new resulting output. You should see the file is now staged for commmit.

6. **Commit**
   
Now, make your commit as follows:

```sh
git commit -asm ":tada:(task1): My first commit."
```

The parameter `-a` refers to "commit all that is staged" and `-s` means "sign the commit with my credentials". `WARNING:` the command above adds an inline message through the usage of the parameter `-m <message>`. This is just to simplify the first task. 
It is advised to get familiar with a terminal editor like `Vim`/`Emacs`/`Nano` as tasks 2-5 will leave the commit message open to you to do.
You will notice that we recommend a commit message be structured as follows:

```sh
<action>(<scope>): <Subject line>

<Message body>

<Message footer>
```

Example:

```sh
update(docker): Added entrypoint.sh.

This commit does away with the default CMD and replaces it with an entrypoint script.

Closes #111
```

7. **Push**
   
Now you are ready to push your commits to your branch. Firstly, make sure your are on your branch by running:

```sh
git branch -a
```

and then when ready, push:

```sh
git push origin feat/task1
```

:tada: Congratulations! You are now done with task 1!



## Task 2

1. **Create a branch for task 2**

Follow the instructions above to create a branch (out of `feat/task1`) and name it `feat/task2`.

2. **Create an Issue**

* Go to issues and click the button "create"
* Pick the template "Feature"
* Give the issue a title as such: `Feature: Enable GitLab-CI builds`.
* Give the issue a description of your choice.
* Explore and add an Assignee and a Label of your choice.

3. **Create a Work In Progress merge request**

* In GitLab, click on your branch (for task 2) and then `Create merge request`.
* Pick the "Feature" template" and make sure the title is: `WIP: Feature: Task 2` 
* Write some description, whatever you want.
* Add to the description `Closes #<issue number>` where `<issue number>` is the number of the issue you created in step 2.
* Explore and add an Assignee and a Label of your choice.
* Make sure to tick the boxes at the bottom of the merge request:
    * [ ] Delete source branch when merge request is accepted.
    * [ ] Squash commits when merge request is accepted.    

4. **Fix CI**

Now, back to your editor. Rename the file `gitlab-ci.not-yml` to `gitlab-ci.yml`.
Stage, commit and push this change to the branch `feat/task2`.
You will now see on your merge request that a commit has been made. `NOTE`: This merge request is still a WIP (Work in Progress). This is indicated by the `WIP` prefix in the title.

By fixing the naming of the `gitlab-ci.yml` file, the CI/CD build process can now be executed. You will notice a build icon appear in the merge request. At this stage, the build will NOT PASS.

5. **Fix the Build**

The build pipeline will give an indication of where the build has failed. Fix the issue and stage, commit and push again.

6. **Merge**

ONLY when the build is successful can you merge the merge request.

The following steps need to be taked:

* Make sure the build succeeded.
* Make sure to remove the `WIP` prefix from the MR title. The title should then just be `Feature: Task 2`.
* Click on the "merge" button.

You should now notice your branch is gone and the issue has been closed. If this is not the case, something went wrong.
Also, you can now manually delete the branch `feat/task1`.


## Task 3

Task 3 is simple - for any prime number `p`, determine the lowest combination of two **prime** numbers `n` and `m` for which 
`
n + m = p
`.

### Constraints
The following constraints will apply:
- A valid value for `p` will always be given - that is, there will be some prime numbers `n` and `m` that sum to `p`.
- `0 <= p <= 1 000 000`.
- `n` should be the lowest possible number possible for testing purposes.

The input will be a single number `p`, and the output should be `n + m = p` followed by a newline. For example:

```2 + 1319 = 1321```

### Testing your solution
When you're happy with your solution, create a branch called `feat/task4` containing your solution and push it to the repository.

Note the following:
- Your solution should be in `task3/task3.py`.
- If you require any Python libraries, add them to the `.gitlab-ci.yml`.

If your solution is correct, the build pipeline will pass.

## Task 4

Task 4 is slightly more complex. Using the numbers below along with 50 public domain books, find the passphrase which will "unlock" the next task.

The following constraints will apply:
- Not all 50 books are required to find the passphrase, but you will need to use at least one of the books.
- You will need to print the all the books you made use of (comma separated).
- You must also print out the passphrase on a new line (space separated).

The output from your program should be in the following format:
```
<book>, <book>, ...
<passphrase>
```

An example of this is:
```
Sense and Sensibility,Emma,Treasure Island
Life is what happens when you are busy making other plans
```

The clues for the decryption are given below: 

```
85,8,
124,11,
1984,8,
3,5,
901,1,
3,13,
8546,12,
5,2,
3,4,
85,10,
3437,7
```

The books required for this task can be found [here](https://drive.google.com/open?id=13_Ii1G_7-Z_kPZ1636asSUha8qQlilAV).

### Testing your solution
When you're happy with your solution, create a branch called `feat/task4` containing your solution and push it to the repository.

Note the following:
- Your solution should be in `task4/task4.py`.
- Any files (such as the books) should be committed and pushed as well.
- If you require any Python libraries, add them to the `.gitlab-ci.yml`.

If your solution is correct, the build pipeline will pass.

## Task 5

Make a Merge Request back into the original Compiax/COS301 repositery. Only do so if all your CI/CD tests are passing to qualify to win some Compiax merchandise.

## Competitive?

Compiax will sponsor some merchandise to the first 3 students who manage to successfully complete all 5 tasks correctly.

<Insert merch designs here>

## Credits

Made with :heart: by Compiax (Pty) Ltd.
